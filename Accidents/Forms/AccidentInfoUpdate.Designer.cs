﻿namespace Accidents
{
    partial class AccidentInfoUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confRegEdit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ListCities = new System.Windows.Forms.ListBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.StatusList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // confRegEdit
            // 
            this.confRegEdit.Location = new System.Drawing.Point(9, 24);
            this.confRegEdit.Margin = new System.Windows.Forms.Padding(2);
            this.confRegEdit.Name = "confRegEdit";
            this.confRegEdit.Size = new System.Drawing.Size(195, 20);
            this.confRegEdit.TabIndex = 0;
            this.confRegEdit.TextChanged += new System.EventHandler(this.confRegEdit_TextChanged);
            this.confRegEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.confRegEdit_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Confirmation registration id";
            // 
            // ListCities
            // 
            this.ListCities.FormattingEnabled = true;
            this.ListCities.Items.AddRange(new object[] {
            "Alabama",
            "Huntsville",
            "Mobile",
            "Montgomery",
            "Alaska",
            "Anchorage",
            "Juneau",
            "Arizona",
            "Flagstaff",
            "Phoenix",
            "Phoenix - virtual tour",
            "Tuscon",
            "Yuma",
            "Arkansas",
            "Little Rock",
            "California",
            "Bakersfield",
            "Concord ",
            "Hollywood ",
            "Los Angeles",
            "Los Angeles - virtual tour ",
            "Orange County",
            "Palm Springs",
            "Sacramento",
            "San Diego",
            "San Francisco",
            "San Francisco - virtual tour ",
            "Santa Barbara",
            "Santa Cruz",
            "Silicon Valley",
            "Colorado",
            "Colorado Springs",
            "Denver",
            "Pueblo",
            "Connecticut",
            "Hartford",
            "New Haven",
            "Delaware",
            "Dover",
            "Wilmington",
            "District of Columbia",
            "See Washington, DC",
            "Washington DC",
            "Washington DC Sightseeing Map",
            "Florida",
            "Daytona",
            "Ft. Lauderdale",
            "Jacksonville",
            "Miami",
            "Miami - virtual tour",
            "Orlando",
            "Orlando - virtual tour",
            "Pensacola",
            "Tallahassee ",
            "Tampa",
            "Tampa - virtual tour",
            "Georgia",
            "Atlanta",
            "Hawaii",
            "Honolulu",
            "Idaho",
            "Boise",
            "Illinois",
            "Chicago",
            "Chicago - virtual tour",
            "Chicago Electronic Tour Guide",
            "Des Plaines",
            "Morrison ",
            "Springfield",
            "Indiana",
            "Indianapolis",
            "Iowa",
            "Davenport",
            "Des Moines",
            "Kansas",
            "Topeka",
            "Wichita",
            "Kentucky",
            "Frankfort ",
            "Lexington",
            "Louisville",
            "Louisiana",
            "Baton Rouge",
            "New Orleans",
            "Shreveport",
            "Maine",
            "Augusta",
            "Portland",
            "Maryland",
            "Annapolis",
            "Baltimore",
            "Massachusetts",
            "Boston",
            "Boston - virtual tour",
            "Michigan",
            "Detroit",
            "Lansing",
            "Northville",
            "Minnesota",
            "Minneapolis",
            "St. Paul",
            "Mississippi",
            "Biloxi",
            "Jackson",
            "Missouri",
            "Jefferson City",
            "Kansas City",
            "St. Joseph",
            "St. Louis",
            "",
            "Montana",
            "Billings",
            "Helena ",
            "Nebraska",
            "Lincoln",
            "Omaha ",
            "Nevada",
            "Carson City",
            "Lake Tahoe - virtual tour",
            "Las Vegas",
            "Las Vegas - virtual tour",
            "Reno ",
            "New Hampshire",
            "Concord",
            "Manchester",
            "New Jersey",
            "Atlantic City",
            "Newark",
            "Trenton ",
            "New Mexico",
            "Albuquerque",
            "Santa Fe",
            "New York",
            "Albany",
            "Buffalo",
            "Long Island",
            "New York:  Bronx",
            "New York:  Brooklyn",
            "New York:  Manhattan",
            "New York:  Manhattan - virtual tour",
            "New York:  Queens",
            "New York City Digital Atlas",
            "New York City Virtual Tour",
            "Niagara Falls",
            "NY Sets Population Record in 2000 Census",
            "Syracuse",
            "North Carolina ",
            "Charlotte",
            "Durham",
            "Fayetteville",
            "Raleigh",
            "North Dakota",
            "Bismarck",
            "Ohio",
            "Cleveland",
            "Columbus",
            "Oklahoma",
            "Oklahoma City",
            "Tulsa",
            "Oregon",
            "Eugene",
            "Portland",
            "Pennsylvania",
            "CITIES OF PENNSYLVANIA - links",
            "Cities of Pennsylvania - Websites",
            "Cities and Towns in Pennsylvania",
            "Allentown Fair",
            "Bloomsburg",
            "Erie",
            "Gettysburg Visitor\'s Guide",
            "Gettysburg Welcome Center",
            "Greater Scranton Chamber of Commerce",
            "Greater Wilkes-Barre Chamber of Commerce",
            "Harrisburg",
            "Jim Thorpe",
            "Lancaster",
            "Philadelphia",
            "Philadelphia SLANGUAGE",
            "Philly Suburbs SLANGUAGE",
            "Pennsylvania Cities",
            "Pittsburgh",
            "Pittsburgh World Firsts",
            "Pittsburgh SLANGUAGE",
            "Pittsburgh Zoo",
            "Scranton - Community Information",
            "Scranton, PA Weather Forecast",
            "Scranton Home Page",
            "Scranton Tomorrow ",
            "SurfScranton",
            "State College",
            "Top 10 Cities in PA",
            "Welcome to SCRANTON OnLine",
            "Wilkes-Barre",
            "Williamsport",
            "Rhode Island",
            "Providence",
            "South Carolina",
            "Charleston",
            "Columbia",
            "Greenville",
            "South Dakota",
            "Pierre",
            "Rapid City",
            "Tennessee",
            "Knoxville",
            "Maryville",
            "Memphis",
            "Nashville",
            "Texas",
            "Austin",
            "Corpus Christi",
            "Dallas",
            "Houston",
            "Tyler",
            "Utah",
            "Salt Lake City",
            "Vermont",
            "Burlington",
            "Montpelier",
            "Virginia",
            "Charlottesville",
            "Hampton Roads",
            "Richmond",
            "Williamsburg, VA",
            "Washington",
            "Olympia",
            "Seattle",
            "Tacoma",
            "West Virginia",
            "Charleston ",
            "Wisconsin",
            "Madison",
            "Milwaukee",
            "Wyoming",
            "Cheyenne",
            "Jackson Hole-Grand Tetons"});
            this.ListCities.Location = new System.Drawing.Point(9, 46);
            this.ListCities.Margin = new System.Windows.Forms.Padding(2);
            this.ListCities.Name = "ListCities";
            this.ListCities.Size = new System.Drawing.Size(195, 199);
            this.ListCities.TabIndex = 2;
            this.ListCities.SelectedIndexChanged += new System.EventHandler(this.ListCities_SelectedIndexChanged);
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(110, 312);
            this.btnDone.Margin = new System.Windows.Forms.Padding(2);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(94, 25);
            this.btnDone.TabIndex = 3;
            this.btnDone.Text = "Save";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(9, 312);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 25);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 251);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Select status";
            // 
            // StatusList
            // 
            this.StatusList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.StatusList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.StatusList.HideSelection = false;
            this.StatusList.Location = new System.Drawing.Point(10, 267);
            this.StatusList.Name = "StatusList";
            this.StatusList.Size = new System.Drawing.Size(192, 40);
            this.StatusList.TabIndex = 11;
            this.StatusList.UseCompatibleStateImageBehavior = false;
            this.StatusList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Status";
            this.columnHeader1.Width = 184;
            // 
            // AccidentInfoUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 346);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.StatusList);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.ListCities);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.confRegEdit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AccidentInfoUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Accident info update";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox confRegEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox ListCities;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView StatusList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}