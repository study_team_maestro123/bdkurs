﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Accidents
{
    public partial class AccidentInfoUpdate : Form
    {

        private int mReferenceId = -1;

        public AccidentInfoUpdate(int referenceId)
        {
            mReferenceId = referenceId;
            InitializeComponent();
            verifySaveButton();

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [StatusTbl]"))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader["statusName"].ToString());
                            item.Tag = reader["pid"];
                            StatusList.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            using (SqlCommand cmd = DbController.Instance.Command(
                "IF NOT EXISTS (SELECT regId FROM [AccidentInfoTbl] WHERE regId=@0) "
                + "INSERT INTO [AccidentInfoTbl] VALUES (@0, @1, @2, @3) "
                + " else "
                + "UPDATE [AccidentInfoTbl] SET confRegId=@1,territory=@2, status=@3 WHERE regId=@0"))
            {
                cmd.Parameters.AddWithValue("@0", mReferenceId);
                cmd.Parameters.AddWithValue("@1", confRegEdit.Text);
                cmd.Parameters.AddWithValue("@2", ListCities.SelectedIndex >= 0 ? ListCities.SelectedItem.ToString() : null);
                cmd.Parameters.AddWithValue("@3", StatusList.SelectedItems.Count > 0 ? StatusList.SelectedItems[0].Tag : -1);

                cmd.ExecuteNonQuery();
            }
            Close();
            DbController.Instance.NotifyDataLoadRequired();
        }

        private void confRegEdit_TextChanged(object sender, EventArgs e)
        {
            verifySaveButton();
        }

        private void ListCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            verifySaveButton();
        }

        private void verifySaveButton()
        {
            btnDone.Enabled = !String.IsNullOrEmpty(confRegEdit.Text) && ListCities.SelectedItem != null && StatusList.SelectedItems.Count > 0;
        }

        private void confRegEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}
