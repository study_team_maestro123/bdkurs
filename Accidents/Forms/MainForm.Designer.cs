﻿namespace Accidents
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListView ListSearchImplications;
            this.tabsControl = new System.Windows.Forms.TabControl();
            this.AccidentsTab = new System.Windows.Forms.TabPage();
            this.ListAccidents = new System.Windows.Forms.ListView();
            this.idColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fabColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dateColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AccidentInfoTab = new System.Windows.Forms.TabPage();
            this.ListAccidentsInfo = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PeoplesTab = new System.Windows.Forms.TabPage();
            this.ListPeoples = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ImplicationsTab = new System.Windows.Forms.TabPage();
            this.ListImplications = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SearchTab = new System.Windows.Forms.TabPage();
            this.ListSearchPeople = new System.Windows.Forms.ListView();
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SearchListAccident = new System.Windows.Forms.ListView();
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SearchEditPeoples = new System.Windows.Forms.TextBox();
            this.SearchEdtAccident = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAccidentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPeopleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addImplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ListSearchImplications = new System.Windows.Forms.ListView();
            this.tabsControl.SuspendLayout();
            this.AccidentsTab.SuspendLayout();
            this.AccidentInfoTab.SuspendLayout();
            this.PeoplesTab.SuspendLayout();
            this.ImplicationsTab.SuspendLayout();
            this.SearchTab.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListSearchImplications
            // 
            ListSearchImplications.Location = new System.Drawing.Point(426, 44);
            ListSearchImplications.Margin = new System.Windows.Forms.Padding(2);
            ListSearchImplications.Name = "ListSearchImplications";
            ListSearchImplications.Size = new System.Drawing.Size(207, 294);
            ListSearchImplications.TabIndex = 8;
            ListSearchImplications.UseCompatibleStateImageBehavior = false;
            // 
            // tabsControl
            // 
            this.tabsControl.Controls.Add(this.AccidentsTab);
            this.tabsControl.Controls.Add(this.AccidentInfoTab);
            this.tabsControl.Controls.Add(this.PeoplesTab);
            this.tabsControl.Controls.Add(this.ImplicationsTab);
            this.tabsControl.Controls.Add(this.SearchTab);
            this.tabsControl.ItemSize = new System.Drawing.Size(204, 30);
            this.tabsControl.Location = new System.Drawing.Point(9, 24);
            this.tabsControl.Margin = new System.Windows.Forms.Padding(2);
            this.tabsControl.Name = "tabsControl";
            this.tabsControl.SelectedIndex = 0;
            this.tabsControl.Size = new System.Drawing.Size(646, 368);
            this.tabsControl.TabIndex = 0;
            this.tabsControl.SelectedIndexChanged += new System.EventHandler(this.tabsControl_SelectedIndexChanged);
            // 
            // AccidentsTab
            // 
            this.AccidentsTab.Controls.Add(this.ListAccidents);
            this.AccidentsTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccidentsTab.Location = new System.Drawing.Point(4, 34);
            this.AccidentsTab.Margin = new System.Windows.Forms.Padding(2);
            this.AccidentsTab.Name = "AccidentsTab";
            this.AccidentsTab.Padding = new System.Windows.Forms.Padding(2);
            this.AccidentsTab.Size = new System.Drawing.Size(638, 330);
            this.AccidentsTab.TabIndex = 0;
            this.AccidentsTab.Text = "Accidents";
            this.AccidentsTab.UseVisualStyleBackColor = true;
            // 
            // ListAccidents
            // 
            this.ListAccidents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListAccidents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idColumn,
            this.fabColumn,
            this.dateColumn});
            this.ListAccidents.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListAccidents.FullRowSelect = true;
            this.ListAccidents.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListAccidents.Location = new System.Drawing.Point(0, 0);
            this.ListAccidents.Margin = new System.Windows.Forms.Padding(0);
            this.ListAccidents.MultiSelect = false;
            this.ListAccidents.Name = "ListAccidents";
            this.ListAccidents.Size = new System.Drawing.Size(636, 330);
            this.ListAccidents.TabIndex = 1;
            this.ListAccidents.UseCompatibleStateImageBehavior = false;
            this.ListAccidents.View = System.Windows.Forms.View.Details;
            this.ListAccidents.DoubleClick += new System.EventHandler(this.OnAccidentDoubleClick);
            // 
            // idColumn
            // 
            this.idColumn.Text = "Registration id";
            this.idColumn.Width = 153;
            // 
            // fabColumn
            // 
            this.fabColumn.Text = "Accident type";
            this.fabColumn.Width = 146;
            // 
            // dateColumn
            // 
            this.dateColumn.Text = "Date";
            this.dateColumn.Width = 106;
            // 
            // AccidentInfoTab
            // 
            this.AccidentInfoTab.Controls.Add(this.ListAccidentsInfo);
            this.AccidentInfoTab.Location = new System.Drawing.Point(4, 34);
            this.AccidentInfoTab.Margin = new System.Windows.Forms.Padding(2);
            this.AccidentInfoTab.Name = "AccidentInfoTab";
            this.AccidentInfoTab.Padding = new System.Windows.Forms.Padding(2);
            this.AccidentInfoTab.Size = new System.Drawing.Size(638, 330);
            this.AccidentInfoTab.TabIndex = 1;
            this.AccidentInfoTab.Text = "Accidents Information";
            this.AccidentInfoTab.UseVisualStyleBackColor = true;
            // 
            // ListAccidentsInfo
            // 
            this.ListAccidentsInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListAccidentsInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader21});
            this.ListAccidentsInfo.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListAccidentsInfo.FullRowSelect = true;
            this.ListAccidentsInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListAccidentsInfo.HideSelection = false;
            this.ListAccidentsInfo.Location = new System.Drawing.Point(0, 0);
            this.ListAccidentsInfo.Margin = new System.Windows.Forms.Padding(0);
            this.ListAccidentsInfo.MultiSelect = false;
            this.ListAccidentsInfo.Name = "ListAccidentsInfo";
            this.ListAccidentsInfo.Size = new System.Drawing.Size(636, 330);
            this.ListAccidentsInfo.TabIndex = 2;
            this.ListAccidentsInfo.UseCompatibleStateImageBehavior = false;
            this.ListAccidentsInfo.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Registration id";
            this.columnHeader1.Width = 96;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Accident type";
            this.columnHeader2.Width = 93;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Date";
            this.columnHeader3.Width = 104;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Confirmation registration id";
            this.columnHeader4.Width = 168;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Territory";
            this.columnHeader5.Width = 80;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Status";
            // 
            // PeoplesTab
            // 
            this.PeoplesTab.Controls.Add(this.ListPeoples);
            this.PeoplesTab.Location = new System.Drawing.Point(4, 34);
            this.PeoplesTab.Margin = new System.Windows.Forms.Padding(2);
            this.PeoplesTab.Name = "PeoplesTab";
            this.PeoplesTab.Padding = new System.Windows.Forms.Padding(2);
            this.PeoplesTab.Size = new System.Drawing.Size(638, 330);
            this.PeoplesTab.TabIndex = 2;
            this.PeoplesTab.Text = "Peoples";
            this.PeoplesTab.UseVisualStyleBackColor = true;
            // 
            // ListPeoples
            // 
            this.ListPeoples.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListPeoples.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.ListPeoples.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListPeoples.FullRowSelect = true;
            this.ListPeoples.Location = new System.Drawing.Point(0, 0);
            this.ListPeoples.Margin = new System.Windows.Forms.Padding(0);
            this.ListPeoples.Name = "ListPeoples";
            this.ListPeoples.Size = new System.Drawing.Size(636, 330);
            this.ListPeoples.TabIndex = 2;
            this.ListPeoples.UseCompatibleStateImageBehavior = false;
            this.ListPeoples.View = System.Windows.Forms.View.Details;
            this.ListPeoples.DoubleClick += new System.EventHandler(this.OnPeopleDoubliClick);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Registration id";
            this.columnHeader6.Width = 105;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Last name";
            this.columnHeader7.Width = 109;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "First name";
            this.columnHeader8.Width = 119;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Patronymic";
            this.columnHeader9.Width = 114;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Adress";
            this.columnHeader10.Width = 173;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Court rate";
            this.columnHeader11.Width = 81;
            // 
            // ImplicationsTab
            // 
            this.ImplicationsTab.Controls.Add(this.ListImplications);
            this.ImplicationsTab.Location = new System.Drawing.Point(4, 34);
            this.ImplicationsTab.Margin = new System.Windows.Forms.Padding(2);
            this.ImplicationsTab.Name = "ImplicationsTab";
            this.ImplicationsTab.Padding = new System.Windows.Forms.Padding(2);
            this.ImplicationsTab.Size = new System.Drawing.Size(638, 330);
            this.ImplicationsTab.TabIndex = 3;
            this.ImplicationsTab.Text = "Implications";
            this.ImplicationsTab.UseVisualStyleBackColor = true;
            // 
            // ListImplications
            // 
            this.ListImplications.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListImplications.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.ListImplications.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListImplications.FullRowSelect = true;
            this.ListImplications.Location = new System.Drawing.Point(0, 0);
            this.ListImplications.Margin = new System.Windows.Forms.Padding(0);
            this.ListImplications.Name = "ListImplications";
            this.ListImplications.Size = new System.Drawing.Size(636, 330);
            this.ListImplications.TabIndex = 2;
            this.ListImplications.UseCompatibleStateImageBehavior = false;
            this.ListImplications.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "People";
            this.columnHeader12.Width = 179;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Status";
            this.columnHeader13.Width = 205;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Confirmation registration id";
            this.columnHeader14.Width = 216;
            // 
            // SearchTab
            // 
            this.SearchTab.Controls.Add(ListSearchImplications);
            this.SearchTab.Controls.Add(this.ListSearchPeople);
            this.SearchTab.Controls.Add(this.SearchListAccident);
            this.SearchTab.Controls.Add(this.label3);
            this.SearchTab.Controls.Add(this.label2);
            this.SearchTab.Controls.Add(this.label1);
            this.SearchTab.Controls.Add(this.textBox3);
            this.SearchTab.Controls.Add(this.SearchEditPeoples);
            this.SearchTab.Controls.Add(this.SearchEdtAccident);
            this.SearchTab.Location = new System.Drawing.Point(4, 34);
            this.SearchTab.Margin = new System.Windows.Forms.Padding(2);
            this.SearchTab.Name = "SearchTab";
            this.SearchTab.Padding = new System.Windows.Forms.Padding(2);
            this.SearchTab.Size = new System.Drawing.Size(638, 330);
            this.SearchTab.TabIndex = 4;
            this.SearchTab.Text = "Search";
            this.SearchTab.UseVisualStyleBackColor = true;
            // 
            // ListSearchPeople
            // 
            this.ListSearchPeople.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.ListSearchPeople.Location = new System.Drawing.Point(215, 44);
            this.ListSearchPeople.Margin = new System.Windows.Forms.Padding(2);
            this.ListSearchPeople.Name = "ListSearchPeople";
            this.ListSearchPeople.Size = new System.Drawing.Size(207, 294);
            this.ListSearchPeople.TabIndex = 7;
            this.ListSearchPeople.UseCompatibleStateImageBehavior = false;
            this.ListSearchPeople.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Lastname";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Firstname";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Patronymic";
            // 
            // SearchListAccident
            // 
            this.SearchListAccident.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.SearchListAccident.Location = new System.Drawing.Point(4, 44);
            this.SearchListAccident.Margin = new System.Windows.Forms.Padding(2);
            this.SearchListAccident.Name = "SearchListAccident";
            this.SearchListAccident.Size = new System.Drawing.Size(207, 294);
            this.SearchListAccident.TabIndex = 6;
            this.SearchListAccident.UseCompatibleStateImageBehavior = false;
            this.SearchListAccident.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Id";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Type";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Implications";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Peoples";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Accidents";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(426, 21);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(207, 20);
            this.textBox3.TabIndex = 2;
            // 
            // SearchEditPeoples
            // 
            this.SearchEditPeoples.Location = new System.Drawing.Point(215, 21);
            this.SearchEditPeoples.Margin = new System.Windows.Forms.Padding(2);
            this.SearchEditPeoples.Name = "SearchEditPeoples";
            this.SearchEditPeoples.Size = new System.Drawing.Size(207, 20);
            this.SearchEditPeoples.TabIndex = 1;
            this.SearchEditPeoples.TextChanged += new System.EventHandler(this.SearchEditPeoples_TextChanged);
            // 
            // SearchEdtAccident
            // 
            this.SearchEdtAccident.Location = new System.Drawing.Point(4, 21);
            this.SearchEdtAccident.Margin = new System.Windows.Forms.Padding(2);
            this.SearchEdtAccident.Name = "SearchEdtAccident";
            this.SearchEdtAccident.Size = new System.Drawing.Size(207, 20);
            this.SearchEdtAccident.TabIndex = 0;
            this.SearchEdtAccident.TextChanged += new System.EventHandler(this.SearchEdtAccident_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.sortToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(664, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAccidentToolStripMenuItem,
            this.addPeopleToolStripMenuItem,
            this.addImplicationToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // addAccidentToolStripMenuItem
            // 
            this.addAccidentToolStripMenuItem.Name = "addAccidentToolStripMenuItem";
            this.addAccidentToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.addAccidentToolStripMenuItem.Text = "Add accident";
            this.addAccidentToolStripMenuItem.Click += new System.EventHandler(this.addAccidentToolStripMenuItem_Click);
            // 
            // addPeopleToolStripMenuItem
            // 
            this.addPeopleToolStripMenuItem.Name = "addPeopleToolStripMenuItem";
            this.addPeopleToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.addPeopleToolStripMenuItem.Text = "Add people";
            this.addPeopleToolStripMenuItem.Click += new System.EventHandler(this.addPeopleToolStripMenuItem_Click);
            // 
            // addImplicationToolStripMenuItem
            // 
            this.addImplicationToolStripMenuItem.Name = "addImplicationToolStripMenuItem";
            this.addImplicationToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.addImplicationToolStripMenuItem.Text = "Add implication";
            this.addImplicationToolStripMenuItem.Click += new System.EventHandler(this.addImplicationToolStripMenuItem_Click);
            // 
            // sortToolStripMenuItem
            // 
            this.sortToolStripMenuItem.Name = "sortToolStripMenuItem";
            this.sortToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.sortToolStripMenuItem.Text = "Sort";
            this.sortToolStripMenuItem.Click += new System.EventHandler(this.sortToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 402);
            this.Controls.Add(this.tabsControl);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accidents";
            this.tabsControl.ResumeLayout(false);
            this.AccidentsTab.ResumeLayout(false);
            this.AccidentInfoTab.ResumeLayout(false);
            this.PeoplesTab.ResumeLayout(false);
            this.ImplicationsTab.ResumeLayout(false);
            this.SearchTab.ResumeLayout(false);
            this.SearchTab.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabsControl;
        private System.Windows.Forms.TabPage AccidentsTab;
        private System.Windows.Forms.TabPage AccidentInfoTab;
        private System.Windows.Forms.TabPage PeoplesTab;
        private System.Windows.Forms.TabPage ImplicationsTab;
        private System.Windows.Forms.TabPage SearchTab;
        private System.Windows.Forms.ListView ListAccidents;
        private System.Windows.Forms.ColumnHeader idColumn;
        private System.Windows.Forms.ColumnHeader fabColumn;
        private System.Windows.Forms.ColumnHeader dateColumn;
        private System.Windows.Forms.ListView ListAccidentsInfo;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ListView ListPeoples;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ListView ListImplications;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAccidentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPeopleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addImplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortToolStripMenuItem;
        private System.Windows.Forms.ListView ListSearchPeople;
        private System.Windows.Forms.ListView SearchListAccident;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox SearchEditPeoples;
        private System.Windows.Forms.TextBox SearchEdtAccident;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
    }
}

