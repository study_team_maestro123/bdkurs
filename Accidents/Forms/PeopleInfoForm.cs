﻿using System.Data.SqlClient;
using System.Windows.Forms;

namespace Accidents.Forms
{
    public partial class PeopleInfoForm : Form
    {

        private int mPeopleId;

        public PeopleInfoForm(int id)
        {
            InitializeComponent();
            mPeopleId = id;

            using (SqlCommand command = DbController.Instance.Command("SELECT TOP 1 * FROM [PeopleTbl] pep WHERE pep.regId = @regId"))
            {
                command.Parameters.AddWithValue("@regId", mPeopleId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        InfoId.Text = "Registration id: " + reader["regId"];
                        InfoLastname.Text = "Lastname: " + reader["lastname"];
                        InfoFirstname.Text = "Firstname: " + reader["firstname"];
                        InfoPatronymic.Text = "Patronymic: " + reader["patronymic"];
                        InfoAdress.Text = "Adress: " + reader["adress"];
                        InfoCourtRate.Text = "Court rate: " + reader["courtRate"];
                    }
                }
            }

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [ImplicationTbl] impl LEFT JOIN [AccidentInfoTbl] info ON (impl.acRegId = info.regId) LEFT JOIN [AccidentTbl] acc ON (impl.acRegId = acc.regId) LEFT JOIN [RelationTbl] rel ON (info.status = rel.pid) WHERE manRegId = @manId"))
            {
                command.Parameters.AddWithValue("@manId", mPeopleId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader["regId"].ToString());
                            item.SubItems.Add(reader["fab"].ToString());
                            item.SubItems.Add(reader["relationName"].ToString());
                            ImplicationsList.Items.Add(item);
                        }
                    }
                }
            }
        }
    }
}
