﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Accidents
{
    public partial class AddPeople : Form
    {

        private int mRegistrationId = -1;

        public AddPeople()
        {
            InitializeComponent();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            using (SqlCommand command = DbController.Instance.Command(
                "IF NOT EXISTS (SELECT regId FROM [PeopleTbl] WHERE regId=@0) "
                + "INSERT INTO [PeopleTbl] VALUES (@1, @2, @3, @4, @5) "
                + " else "
                + "UPDATE [PeopleTbl] SET lastname=@1,firstname=@2,patronymic=@3,adress=@4,courtRate=@5 WHERE regId=@0"))
            {
                command.Parameters.AddWithValue("@0", mRegistrationId);
                command.Parameters.AddWithValue("@1", lastNameEdit.Text);
                command.Parameters.AddWithValue("@2", firstNameEdit.Text);
                command.Parameters.AddWithValue("@3", patronymicEdit.Text);
                command.Parameters.AddWithValue("@4", adressEdit.Text);
                command.Parameters.AddWithValue("@5", courtRateEdit.Text);

                command.ExecuteNonQuery();
                DbController.Instance.NotifyDataLoadRequired();
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
