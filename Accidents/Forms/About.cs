﻿using System;
using System.Windows.Forms;

namespace Accidents.Forms
{
    partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}