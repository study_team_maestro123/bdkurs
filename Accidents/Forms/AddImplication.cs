﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Accidents.Forms
{   
    public partial class AddImplication : Form
    {
        private int mAccidentId = -1;

        public AddImplication(int accidentId)
        {
            InitializeComponent();

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [RelationTbl]"))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows) {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader.GetValue(1).ToString());
                            item.Tag = reader.GetValue(0);
                            relationList.Items.Add(item);
                        }
                    }
                }
            }

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [PeopleTbl]"))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader.GetValue(1).ToString());
                            item.Tag = reader.GetValue(0);
                            item.SubItems.Add(reader.GetValue(2).ToString());
                            item.SubItems.Add(reader.GetValue(3).ToString());
                            listPeoples.Items.Add(item);
                        }
                    }
                }
            }

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [AccidentInfoTbl]"))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ListViewItem item = new ListViewItem(reader.GetValue(1).ToString());
                        item.SubItems.Add(reader.GetValue(2).ToString());
                        item.Tag = reader.GetValue(0);
                        item.Selected = mAccidentId == (int) item.Tag;
                        accList.Items.Add(item);

                    }
                }

                accList.Select();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            using (SqlCommand command = DbController.Instance.Command("INSERT INTO [ImplicationTbl] VALUES (@0,@1,@2)"))
            {
                command.Parameters.AddWithValue("@0", relationList.SelectedItems[0].Tag);
                command.Parameters.AddWithValue("@1", accList.SelectedItems[0].Tag);
                command.Parameters.AddWithValue("@2", listPeoples.SelectedItems[0].Tag);

                command.ExecuteNonQuery();
                DbController.Instance.NotifyDataLoadRequired();
                Close();
            }
        }
    }
}
