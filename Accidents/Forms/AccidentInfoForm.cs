﻿using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;

namespace Accidents.Forms
{
    public partial class AccidentInfoForm : Form
    {

        private int mAccidentId;

        public AccidentInfoForm(int accidentId)
        {
            InitializeComponent();
            mAccidentId = accidentId;
            Debug.WriteLine("mAccident: " + mAccidentId);
            using (SqlCommand command = DbController.Instance.Command("SELECT TOP 1 * FROM [AccidentTbl] acc LEFT JOIN [AccidentInfoTbl] info ON (info.regId = acc.regId) LEFT JOIN [StatusTbl] st ON (info.status = st.pid) WHERE acc.regId = @regId"))
            {
                command.Parameters.AddWithValue("@regId", mAccidentId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Debug.WriteLine("has rows: " + reader.HasRows);
                    if (reader.HasRows)
                    {
                        reader.Read();
                        InfoRegId.Text = "Registration id: " + reader["regId"];
                        InfoType.Text = "Type: " + reader["fab"];
                        InfoDate.Text = "Date: " + reader["regDate"];
                        InfoConfId.Text = "Confirmation id: " + reader["confRegId"];
                        InfoTerritory.Text = "Territory: " + reader["territory"];
                        InfoStatus.Text = "Status: " + reader["statusName"];
                    }
                    else
                    {
                        MessageBox.Show("No accident found!");
                        Close();
                    }
                }
            }

            using (SqlCommand command = DbController.Instance.Command("SELECT * FROM [ImplicationTbl] impl LEFT JOIN [PeopleTbl] people ON (impl.manRegId = people.regId) LEFT JOIN [RelationTbl] rel ON (impl.status = rel.pid) WHERE (impl.acRegId = @regId)"))
            {
                command.Parameters.AddWithValue("@regId", mAccidentId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem();
                            item.Text = reader["lastname"] + " " + reader["firstname"] + " " + reader["patronymic"];
                            item.SubItems.Add(reader["relationName"].ToString());
                            PeoplesList.Items.Add(item);
                        }
                    }
                }
            }

        }
        
    }
}
