﻿namespace Accidents.Forms
{
    partial class AccidentInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.InfoRegId = new System.Windows.Forms.Label();
            this.InfoType = new System.Windows.Forms.Label();
            this.InfoDate = new System.Windows.Forms.Label();
            this.InfoConfId = new System.Windows.Forms.Label();
            this.InfoTerritory = new System.Windows.Forms.Label();
            this.InfoStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PeoplesList = new System.Windows.Forms.ListView();
            this.People = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Relation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Accident Info";
            // 
            // InfoRegId
            // 
            this.InfoRegId.AutoSize = true;
            this.InfoRegId.Location = new System.Drawing.Point(9, 32);
            this.InfoRegId.Name = "InfoRegId";
            this.InfoRegId.Size = new System.Drawing.Size(70, 13);
            this.InfoRegId.TabIndex = 1;
            this.InfoRegId.Text = "Accident Info";
            // 
            // InfoType
            // 
            this.InfoType.AutoSize = true;
            this.InfoType.Location = new System.Drawing.Point(9, 54);
            this.InfoType.Name = "InfoType";
            this.InfoType.Size = new System.Drawing.Size(70, 13);
            this.InfoType.TabIndex = 2;
            this.InfoType.Text = "Accident Info";
            // 
            // InfoDate
            // 
            this.InfoDate.AutoSize = true;
            this.InfoDate.Location = new System.Drawing.Point(9, 76);
            this.InfoDate.Name = "InfoDate";
            this.InfoDate.Size = new System.Drawing.Size(70, 13);
            this.InfoDate.TabIndex = 3;
            this.InfoDate.Text = "Accident Info";
            // 
            // InfoConfId
            // 
            this.InfoConfId.AutoSize = true;
            this.InfoConfId.Location = new System.Drawing.Point(9, 98);
            this.InfoConfId.Name = "InfoConfId";
            this.InfoConfId.Size = new System.Drawing.Size(70, 13);
            this.InfoConfId.TabIndex = 4;
            this.InfoConfId.Text = "Accident Info";
            // 
            // InfoTerritory
            // 
            this.InfoTerritory.AutoSize = true;
            this.InfoTerritory.Location = new System.Drawing.Point(9, 120);
            this.InfoTerritory.Name = "InfoTerritory";
            this.InfoTerritory.Size = new System.Drawing.Size(70, 13);
            this.InfoTerritory.TabIndex = 5;
            this.InfoTerritory.Text = "Accident Info";
            // 
            // InfoStatus
            // 
            this.InfoStatus.AutoSize = true;
            this.InfoStatus.Location = new System.Drawing.Point(9, 142);
            this.InfoStatus.Name = "InfoStatus";
            this.InfoStatus.Size = new System.Drawing.Size(70, 13);
            this.InfoStatus.TabIndex = 6;
            this.InfoStatus.Text = "Accident Info";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Implication Peoples";
            // 
            // PeoplesList
            // 
            this.PeoplesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.People,
            this.Relation});
            this.PeoplesList.Location = new System.Drawing.Point(12, 198);
            this.PeoplesList.Name = "PeoplesList";
            this.PeoplesList.Size = new System.Drawing.Size(382, 134);
            this.PeoplesList.TabIndex = 9;
            this.PeoplesList.UseCompatibleStateImageBehavior = false;
            this.PeoplesList.View = System.Windows.Forms.View.Details;
            // 
            // People
            // 
            this.People.Text = "People";
            this.People.Width = 305;
            // 
            // Relation
            // 
            this.Relation.Text = "Relation";
            this.Relation.Width = 69;
            // 
            // AccidentInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 346);
            this.Controls.Add(this.PeoplesList);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.InfoStatus);
            this.Controls.Add(this.InfoTerritory);
            this.Controls.Add(this.InfoConfId);
            this.Controls.Add(this.InfoDate);
            this.Controls.Add(this.InfoType);
            this.Controls.Add(this.InfoRegId);
            this.Controls.Add(this.label1);
            this.Name = "AccidentInfoForm";
            this.Text = "Accident info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label InfoRegId;
        private System.Windows.Forms.Label InfoType;
        private System.Windows.Forms.Label InfoDate;
        private System.Windows.Forms.Label InfoConfId;
        private System.Windows.Forms.Label InfoTerritory;
        private System.Windows.Forms.Label InfoStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView PeoplesList;
        private System.Windows.Forms.ColumnHeader People;
        private System.Windows.Forms.ColumnHeader Relation;
    }
}