﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Accidents.Forms
{
    public partial class SortWindow : Form
    {
        public SortWindow()
        {
            InitializeComponent();
        }

        private void RadioAccident_Click(object sender, EventArgs args)
        {
            Debug.WriteLine("Sender: " + sender);
            if (RadioAccidentId != sender)
            {
                RadioAccidentId.Checked = false;
            }
            else
            {
                DbController.Instance.setAccidentSort(DbController.AccidentSort.Id);
            }
            if (RadioAccidentName != sender)
            {
                RadioAccidentName.Checked = false;
            }
            else
            {
                DbController.Instance.setAccidentSort(DbController.AccidentSort.Type);
            }
            if (RadioAccidentDate != sender)
            {
                RadioAccidentDate.Checked = false;
            }
            else
            {
                DbController.Instance.setAccidentSort(DbController.AccidentSort.Date);
            }
            ((RadioButton)sender).Checked = true;
        }

        private void RadioPeople_Click(object sender, EventArgs args)
        {
            Debug.WriteLine("Sender: " + ((RadioButton)sender).Name);
            if (RadioPeopleId != sender)
            {
                RadioPeopleId.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.Id);
            }
            if (RadioPeopleLastname != sender)
            {
                RadioPeopleLastname.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.Lastname);
            }
            if (RadioPeopleFirstname != sender)
            {
                RadioPeopleFirstname.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.Firstname);
            }
            if (RadioPeoplePatronymic != sender)
            {
                RadioPeoplePatronymic.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.Patronymic);
            }
            if (RadioPeopleAdress != sender)
            {
                RadioPeopleAdress.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.Adress);
            }
            if (ReadioPeopleCourtRate != sender)
            {
                ReadioPeopleCourtRate.Checked = false;
            }
            else
            {
                DbController.Instance.setPeopleSort(DbController.PeopleSort.CourtRate);
            }
            ((RadioButton)sender).Checked = true;
        }

    }
}
