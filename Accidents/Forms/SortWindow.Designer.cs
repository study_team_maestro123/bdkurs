﻿namespace Accidents.Forms
{
    partial class SortWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.RadioAccidentId = new System.Windows.Forms.RadioButton();
            this.RadioAccidentName = new System.Windows.Forms.RadioButton();
            this.RadioAccidentDate = new System.Windows.Forms.RadioButton();
            this.RadioPeopleLastname = new System.Windows.Forms.RadioButton();
            this.RadioPeopleFirstname = new System.Windows.Forms.RadioButton();
            this.RadioPeopleId = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.RadioPeoplePatronymic = new System.Windows.Forms.RadioButton();
            this.RadioPeopleAdress = new System.Windows.Forms.RadioButton();
            this.ReadioPeopleCourtRate = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Accident sort:";
            // 
            // RadioAccidentId
            // 
            this.RadioAccidentId.AutoSize = true;
            this.RadioAccidentId.Location = new System.Drawing.Point(9, 29);
            this.RadioAccidentId.Margin = new System.Windows.Forms.Padding(2);
            this.RadioAccidentId.Name = "RadioAccidentId";
            this.RadioAccidentId.Size = new System.Drawing.Size(70, 17);
            this.RadioAccidentId.TabIndex = 1;
            this.RadioAccidentId.TabStop = true;
            this.RadioAccidentId.Text = "Sort by Id";
            this.RadioAccidentId.UseVisualStyleBackColor = true;
            this.RadioAccidentId.Click += new System.EventHandler(this.RadioAccident_Click);
            // 
            // RadioAccidentName
            // 
            this.RadioAccidentName.AutoSize = true;
            this.RadioAccidentName.Location = new System.Drawing.Point(9, 51);
            this.RadioAccidentName.Margin = new System.Windows.Forms.Padding(2);
            this.RadioAccidentName.Name = "RadioAccidentName";
            this.RadioAccidentName.Size = new System.Drawing.Size(81, 17);
            this.RadioAccidentName.TabIndex = 2;
            this.RadioAccidentName.TabStop = true;
            this.RadioAccidentName.Text = "Sort by type";
            this.RadioAccidentName.UseVisualStyleBackColor = true;
            this.RadioAccidentName.Click += new System.EventHandler(this.RadioAccident_Click);
            // 
            // RadioAccidentDate
            // 
            this.RadioAccidentDate.AutoSize = true;
            this.RadioAccidentDate.Location = new System.Drawing.Point(9, 73);
            this.RadioAccidentDate.Margin = new System.Windows.Forms.Padding(2);
            this.RadioAccidentDate.Name = "RadioAccidentDate";
            this.RadioAccidentDate.Size = new System.Drawing.Size(82, 17);
            this.RadioAccidentDate.TabIndex = 3;
            this.RadioAccidentDate.TabStop = true;
            this.RadioAccidentDate.Text = "Sort by date";
            this.RadioAccidentDate.UseVisualStyleBackColor = true;
            this.RadioAccidentDate.Click += new System.EventHandler(this.RadioAccident_Click);
            // 
            // RadioPeopleLastname
            // 
            this.RadioPeopleLastname.AutoSize = true;
            this.RadioPeopleLastname.Location = new System.Drawing.Point(9, 166);
            this.RadioPeopleLastname.Margin = new System.Windows.Forms.Padding(2);
            this.RadioPeopleLastname.Name = "RadioPeopleLastname";
            this.RadioPeopleLastname.Size = new System.Drawing.Size(103, 17);
            this.RadioPeopleLastname.TabIndex = 7;
            this.RadioPeopleLastname.TabStop = true;
            this.RadioPeopleLastname.Text = "Sort by lastname";
            this.RadioPeopleLastname.UseVisualStyleBackColor = true;
            this.RadioPeopleLastname.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // RadioPeopleFirstname
            // 
            this.RadioPeopleFirstname.AutoSize = true;
            this.RadioPeopleFirstname.Location = new System.Drawing.Point(9, 144);
            this.RadioPeopleFirstname.Margin = new System.Windows.Forms.Padding(2);
            this.RadioPeopleFirstname.Name = "RadioPeopleFirstname";
            this.RadioPeopleFirstname.Size = new System.Drawing.Size(103, 17);
            this.RadioPeopleFirstname.TabIndex = 6;
            this.RadioPeopleFirstname.TabStop = true;
            this.RadioPeopleFirstname.Text = "Sort by firstname";
            this.RadioPeopleFirstname.UseVisualStyleBackColor = true;
            this.RadioPeopleFirstname.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // RadioPeopleId
            // 
            this.RadioPeopleId.AutoSize = true;
            this.RadioPeopleId.Location = new System.Drawing.Point(9, 122);
            this.RadioPeopleId.Margin = new System.Windows.Forms.Padding(2);
            this.RadioPeopleId.Name = "RadioPeopleId";
            this.RadioPeopleId.Size = new System.Drawing.Size(70, 17);
            this.RadioPeopleId.TabIndex = 5;
            this.RadioPeopleId.TabStop = true;
            this.RadioPeopleId.Text = "Sort by Id";
            this.RadioPeopleId.UseVisualStyleBackColor = true;
            this.RadioPeopleId.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 100);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Peoples sort:";
            // 
            // RadioPeoplePatronymic
            // 
            this.RadioPeoplePatronymic.AutoSize = true;
            this.RadioPeoplePatronymic.Location = new System.Drawing.Point(9, 189);
            this.RadioPeoplePatronymic.Margin = new System.Windows.Forms.Padding(2);
            this.RadioPeoplePatronymic.Name = "RadioPeoplePatronymic";
            this.RadioPeoplePatronymic.Size = new System.Drawing.Size(112, 17);
            this.RadioPeoplePatronymic.TabIndex = 8;
            this.RadioPeoplePatronymic.TabStop = true;
            this.RadioPeoplePatronymic.Text = "Sort by patronymic";
            this.RadioPeoplePatronymic.UseVisualStyleBackColor = true;
            this.RadioPeoplePatronymic.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // RadioPeopleAdress
            // 
            this.RadioPeopleAdress.AutoSize = true;
            this.RadioPeopleAdress.Location = new System.Drawing.Point(9, 212);
            this.RadioPeopleAdress.Margin = new System.Windows.Forms.Padding(2);
            this.RadioPeopleAdress.Name = "RadioPeopleAdress";
            this.RadioPeopleAdress.Size = new System.Drawing.Size(92, 17);
            this.RadioPeopleAdress.TabIndex = 9;
            this.RadioPeopleAdress.TabStop = true;
            this.RadioPeopleAdress.Text = "Sort by adress";
            this.RadioPeopleAdress.UseVisualStyleBackColor = true;
            this.RadioPeopleAdress.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // ReadioPeopleCourtRate
            // 
            this.ReadioPeopleCourtRate.AutoSize = true;
            this.ReadioPeopleCourtRate.Location = new System.Drawing.Point(9, 234);
            this.ReadioPeopleCourtRate.Margin = new System.Windows.Forms.Padding(2);
            this.ReadioPeopleCourtRate.Name = "ReadioPeopleCourtRate";
            this.ReadioPeopleCourtRate.Size = new System.Drawing.Size(106, 17);
            this.ReadioPeopleCourtRate.TabIndex = 10;
            this.ReadioPeopleCourtRate.TabStop = true;
            this.ReadioPeopleCourtRate.Text = "Sort by court rate";
            this.ReadioPeopleCourtRate.UseVisualStyleBackColor = true;
            this.ReadioPeopleCourtRate.Click += new System.EventHandler(this.RadioPeople_Click);
            // 
            // SortWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(128, 262);
            this.Controls.Add(this.ReadioPeopleCourtRate);
            this.Controls.Add(this.RadioPeopleAdress);
            this.Controls.Add(this.RadioPeoplePatronymic);
            this.Controls.Add(this.RadioPeopleLastname);
            this.Controls.Add(this.RadioPeopleFirstname);
            this.Controls.Add(this.RadioPeopleId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RadioAccidentDate);
            this.Controls.Add(this.RadioAccidentName);
            this.Controls.Add(this.RadioAccidentId);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SortWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sort";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RadioAccidentId;
        private System.Windows.Forms.RadioButton RadioAccidentName;
        private System.Windows.Forms.RadioButton RadioAccidentDate;
        private System.Windows.Forms.RadioButton RadioPeopleLastname;
        private System.Windows.Forms.RadioButton RadioPeopleFirstname;
        private System.Windows.Forms.RadioButton RadioPeopleId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton RadioPeoplePatronymic;
        private System.Windows.Forms.RadioButton RadioPeopleAdress;
        private System.Windows.Forms.RadioButton ReadioPeopleCourtRate;
    }
}