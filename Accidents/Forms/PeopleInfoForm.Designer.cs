﻿namespace Accidents.Forms
{
    partial class PeopleInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.InfoId = new System.Windows.Forms.Label();
            this.InfoLastname = new System.Windows.Forms.Label();
            this.InfoFirstname = new System.Windows.Forms.Label();
            this.InfoPatronymic = new System.Windows.Forms.Label();
            this.InfoAdress = new System.Windows.Forms.Label();
            this.InfoCourtRate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ImplicationsList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "People Info";
            // 
            // InfoId
            // 
            this.InfoId.AutoSize = true;
            this.InfoId.Location = new System.Drawing.Point(12, 31);
            this.InfoId.Name = "InfoId";
            this.InfoId.Size = new System.Drawing.Size(35, 13);
            this.InfoId.TabIndex = 1;
            this.InfoId.Text = "label2";
            // 
            // InfoLastname
            // 
            this.InfoLastname.AutoSize = true;
            this.InfoLastname.Location = new System.Drawing.Point(12, 53);
            this.InfoLastname.Name = "InfoLastname";
            this.InfoLastname.Size = new System.Drawing.Size(35, 13);
            this.InfoLastname.TabIndex = 2;
            this.InfoLastname.Text = "label3";
            // 
            // InfoFirstname
            // 
            this.InfoFirstname.AutoSize = true;
            this.InfoFirstname.Location = new System.Drawing.Point(12, 75);
            this.InfoFirstname.Name = "InfoFirstname";
            this.InfoFirstname.Size = new System.Drawing.Size(35, 13);
            this.InfoFirstname.TabIndex = 3;
            this.InfoFirstname.Text = "label4";
            // 
            // InfoPatronymic
            // 
            this.InfoPatronymic.AutoSize = true;
            this.InfoPatronymic.Location = new System.Drawing.Point(12, 97);
            this.InfoPatronymic.Name = "InfoPatronymic";
            this.InfoPatronymic.Size = new System.Drawing.Size(35, 13);
            this.InfoPatronymic.TabIndex = 4;
            this.InfoPatronymic.Text = "label5";
            // 
            // InfoAdress
            // 
            this.InfoAdress.AutoSize = true;
            this.InfoAdress.Location = new System.Drawing.Point(12, 119);
            this.InfoAdress.Name = "InfoAdress";
            this.InfoAdress.Size = new System.Drawing.Size(35, 13);
            this.InfoAdress.TabIndex = 5;
            this.InfoAdress.Text = "label6";
            // 
            // InfoCourtRate
            // 
            this.InfoCourtRate.AutoSize = true;
            this.InfoCourtRate.Location = new System.Drawing.Point(12, 141);
            this.InfoCourtRate.Name = "InfoCourtRate";
            this.InfoCourtRate.Size = new System.Drawing.Size(35, 13);
            this.InfoCourtRate.TabIndex = 6;
            this.InfoCourtRate.Text = "label7";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Implications";
            // 
            // ImplicationsList
            // 
            this.ImplicationsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.ImplicationsList.Location = new System.Drawing.Point(12, 179);
            this.ImplicationsList.Name = "ImplicationsList";
            this.ImplicationsList.Size = new System.Drawing.Size(243, 132);
            this.ImplicationsList.TabIndex = 8;
            this.ImplicationsList.UseCompatibleStateImageBehavior = false;
            this.ImplicationsList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Accident id";
            this.columnHeader1.Width = 71;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Type";
            this.columnHeader2.Width = 78;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 79;
            // 
            // PeopleInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 324);
            this.Controls.Add(this.ImplicationsList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.InfoCourtRate);
            this.Controls.Add(this.InfoAdress);
            this.Controls.Add(this.InfoPatronymic);
            this.Controls.Add(this.InfoFirstname);
            this.Controls.Add(this.InfoLastname);
            this.Controls.Add(this.InfoId);
            this.Controls.Add(this.label1);
            this.Name = "PeopleInfoForm";
            this.Text = "People Info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label InfoId;
        private System.Windows.Forms.Label InfoLastname;
        private System.Windows.Forms.Label InfoFirstname;
        private System.Windows.Forms.Label InfoPatronymic;
        private System.Windows.Forms.Label InfoAdress;
        private System.Windows.Forms.Label InfoCourtRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView ImplicationsList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
    }
}