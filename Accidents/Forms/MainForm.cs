﻿using Accidents.Forms;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Accidents
{
    public partial class MainForm : Form, DbController.OnDataLoadRequiredListener
    {
        public MainForm()
        {
            InitializeComponent();
            ListAccidents.MouseClick += OnAccidentMouseClick;
            ListAccidents.MouseDoubleClick += OnAccidentClick;
            ListAccidentsInfo.MouseClick += OnAccidentMouseClick;
            ListPeoples.MouseClick += OnAccidentMouseClick;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            DbController.Instance.Start();
            DbController.Instance.AddListener(this);
            OnDataLoadRequired();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            DbController.Instance.Stop();
            DbController.Instance.RemoveListener(this);
        }

        private void BtnAddAccident_Click(object sender, EventArgs e)
        {
            new AccidentCreateForm().ShowDialog();
        }

        public void OnDataLoadRequired()
        {
            LoadPeoplesData(ListPeoples, null, false);
            LoadAccidentData(ListAccidents, null);
            LoadAccidentInfoData();
            LoadImplicationsData();
        }

        private void LoadAccidentData(ListView list, String query)
        {
            list.Items.Clear();
            String sqlCommand = String.IsNullOrEmpty(query) ? DbController.QUERY_ACCIDENTS : "SELECT * FROM [AccidentTbl] WHERE (fab LIKE @q)";
            using (SqlCommand command = DbController.Instance.Command(sqlCommand + " ORDER BY " + DbController.AccidentSortSql(DbController.Instance.getAccidentSort())))
            {
                if (!String.IsNullOrEmpty(query))
                {
                    command.Parameters.AddWithValue("@q", "%" + query + "%");
                }

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader.GetValue(0).ToString());
                            item.SubItems.Add(reader.GetValue(2).ToString());
                            item.SubItems.Add(reader.GetDateTime(1).ToString());
                            item.Tag = reader["regId"];
                            list.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void LoadAccidentInfoData()
        {
            ListAccidentsInfo.Items.Clear();
            using (SqlCommand command = DbController.Instance.Command(DbController.QUERY_ACCIDENTS_INFO))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader["regId"].ToString());
                            item.SubItems.Add(reader["fab"].ToString());
                            item.SubItems.Add(reader["regDate"].ToString());
                            item.SubItems.Add(reader["confRegId"].ToString());
                            item.SubItems.Add(reader["territory"].ToString());
                            item.SubItems.Add(reader["statusName"].ToString());
                            ListAccidentsInfo.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void OnAccidentClick(object sender, MouseEventArgs args)
        {
            ListViewHitTestInfo info = ListAccidents.HitTest(args.Location);
            if (info.Item != null || info.SubItem != null)
            {
                new AccidentInfoUpdate((int) info.Item.Tag).ShowDialog();
            }
        }

        private void OnAccidentMouseClick(object sender, MouseEventArgs args)
        {
            Debug.WriteLine("OnMouseClick");
            if (args.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo info = ((ListView) sender).HitTest(args.Location);
                if (info.Item != null || info.SubItem != null)
                {
                    openContextMenu((Control) sender, info.Item.Text, args.X, args.Y, sender == ListAccidentsInfo);
                }
            }
        }

        private void openContextMenu(Control control, object tagObject, int x, int y, bool withPeopleAdd)
        {
            MenuItem updateItem = new MenuItem(control == ListAccidentsInfo ? "Update" : "Confirm");
            updateItem.Click += new EventHandler(OnUpdateInfoContextItemClicked);
            updateItem.Tag = tagObject;
            MenuItem deleteItem = new MenuItem("Delete");
            deleteItem.Click += new EventHandler(OnAccDeleteClick);
            deleteItem.Break = true;
            deleteItem.Tag = tagObject;
            ContextMenu menu = new ContextMenu();
            menu.MenuItems.Add(updateItem);

            if (withPeopleAdd)
            {
                MenuItem item = new MenuItem("Add implication");
                item.Click += new EventHandler(openImplicationDialog);
                menu.MenuItems.Add(item);
            }

            menu.MenuItems.Add(deleteItem);
            menu.Tag = control;
            menu.Show(control, new Point(x, y));
        }

        private void OnAccDeleteClick(object sender, EventArgs args)
        {
            MenuItem item = (MenuItem) sender;
            int id = Int32.Parse(item.Tag.ToString());
            if (item.Parent.Tag == ListPeoples)
            {
                using (SqlCommand command = DbController.Instance.Command("DELETE FROM [PeopleTbl] WHERE regId=@0"))
                {
                    command.Parameters.AddWithValue("@0", id);
                    command.ExecuteNonQuery();
                    DbController.Instance.NotifyDataLoadRequired();
                }
            }
            else if(item.Parent.Tag == ListAccidentsInfo)
            {
                using (SqlCommand command = DbController.Instance.Command("DELETE FROM [AccidentInfoTbl] WHERE regId=@0"))
                {
                    command.Parameters.AddWithValue("@0", id);
                    command.ExecuteNonQuery();
                    DbController.Instance.NotifyDataLoadRequired();
                }
            }
            else
            {
                using (SqlCommand command = DbController.Instance.Command("DELETE FROM [AccidentTbl] WHERE regId=@0"))
                {
                    command.Parameters.AddWithValue("@0", id);
                    command.ExecuteNonQuery();
                    DbController.Instance.NotifyDataLoadRequired();
                }
            }
        }

        private void OnUpdateInfoContextItemClicked(object sender, EventArgs args)
        {
            int id = Int32.Parse(((MenuItem)sender).Tag.ToString());
            new AccidentInfoUpdate(id).ShowDialog();
        }

        private void openImplicationDialog(object sender, EventArgs args)
        {
            new AddImplication(-1).ShowDialog();
        }

        private void LoadPeoplesData(ListView list, String query, bool isSearch)
        {
            bool haveQuery = !String.IsNullOrEmpty(query);
            String sqlCommand = !haveQuery ? DbController.QUERY_PEOPLES : "SELECT * FROM [PeopleTbl] WHERE (lastname LIKE @q OR firstname LIKE @q OR patronymic LIKE @q)";

            list.Items.Clear();
            using (SqlCommand command = DbController.Instance.Command(DbController.QUERY_PEOPLES + " ORDER BY " + DbController.PeopleSortSql(DbController.Instance.getPeopleSort())))
            {
                if (haveQuery)
                {
                    command.Parameters.AddWithValue("@q", "%" + query + "%");
                }
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        int startIndex = isSearch ? 1 : 0;
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader.GetValue(startIndex).ToString());
                            item.SubItems.Add(reader.GetValue(startIndex + 1).ToString());
                            item.SubItems.Add(reader.GetValue(startIndex + 2).ToString());
                            if (!isSearch)
                            { 
                                item.SubItems.Add(reader.GetValue(startIndex + 3).ToString());
                                item.SubItems.Add(reader.GetValue(startIndex + 4).ToString());
                                item.SubItems.Add(reader.GetValue(startIndex + 5).ToString());
                            }
                            item.Tag = reader["regId"];
                            list.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void LoadImplicationsData()
        {
            ListImplications.Items.Clear();
            using (SqlCommand command = DbController.Instance.Command(DbController.QUERY_IMPLICATIONS))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListViewItem item = new ListViewItem(reader["lastname"].ToString() + " " + reader["firstname"].ToString() + " " + reader["patronymic"].ToString());
                            item.SubItems.Add(reader["relationName"].ToString());
                            item.SubItems.Add(reader["confRegId"].ToString());
                            ListImplications.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void addImplication_Click(object sender, EventArgs e)
        {
            new AddImplication(-1).ShowDialog();
        }

        private void addAccidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AccidentCreateForm().ShowDialog();
        }

        private void addPeopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AddPeople().ShowDialog();
        }

        private void addImplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AddImplication(-1).ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new About().ShowDialog();
        }

        private void sortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SortWindow().ShowDialog();
        }

        private void SearchEdtAccident_TextChanged(object sender, EventArgs e)
        {
            SearchAccident(SearchEdtAccident.Text);
        }

        private void SearchAccident(String query) {
            LoadAccidentData(SearchListAccident, query);
        }

        private void SearchPeoples(String query) {
            LoadPeoplesData(ListSearchPeople, query, true);
        }

        private void tabsControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabsControl.SelectedIndex == 4)
            {
                SearchAccident(null);
                SearchPeoples(null);
            }
        }

        private void SearchEditPeoples_TextChanged(object sender, EventArgs e)
        {
            SearchPeoples(SearchEditPeoples.Text);
        }

        private void OnAccidentDoubleClick(object sender, EventArgs e)
        {
            Debug.WriteLine("Selected Text: " + ListAccidents.SelectedItems[0].Text);
            Debug.WriteLine("Selected tag: " + ListAccidents.SelectedItems[0].Tag);
            new AccidentInfoForm((int) ListAccidents.SelectedItems[0].Tag).ShowDialog();
        }

        private void OnPeopleDoubliClick(object sender, EventArgs e)
        {
            new PeopleInfoForm((int)ListPeoples.SelectedItems[0].Tag).ShowDialog();
        }
    }
}
