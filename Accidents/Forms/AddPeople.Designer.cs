﻿namespace Accidents
{
    partial class AddPeople
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lastNameEdit = new System.Windows.Forms.TextBox();
            this.firstNameEdit = new System.Windows.Forms.TextBox();
            this.patronymicEdit = new System.Windows.Forms.TextBox();
            this.adressEdit = new System.Windows.Forms.TextBox();
            this.courtRateEdit = new System.Windows.Forms.TextBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lastNameEdit
            // 
            this.lastNameEdit.Location = new System.Drawing.Point(9, 27);
            this.lastNameEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lastNameEdit.Name = "lastNameEdit";
            this.lastNameEdit.Size = new System.Drawing.Size(174, 20);
            this.lastNameEdit.TabIndex = 0;
            // 
            // firstNameEdit
            // 
            this.firstNameEdit.Location = new System.Drawing.Point(9, 72);
            this.firstNameEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.firstNameEdit.Name = "firstNameEdit";
            this.firstNameEdit.Size = new System.Drawing.Size(174, 20);
            this.firstNameEdit.TabIndex = 1;
            // 
            // patronymicEdit
            // 
            this.patronymicEdit.Location = new System.Drawing.Point(9, 121);
            this.patronymicEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patronymicEdit.Name = "patronymicEdit";
            this.patronymicEdit.Size = new System.Drawing.Size(174, 20);
            this.patronymicEdit.TabIndex = 2;
            // 
            // adressEdit
            // 
            this.adressEdit.Location = new System.Drawing.Point(9, 171);
            this.adressEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.adressEdit.Name = "adressEdit";
            this.adressEdit.Size = new System.Drawing.Size(174, 20);
            this.adressEdit.TabIndex = 3;
            // 
            // courtRateEdit
            // 
            this.courtRateEdit.Location = new System.Drawing.Point(9, 219);
            this.courtRateEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.courtRateEdit.Name = "courtRateEdit";
            this.courtRateEdit.Size = new System.Drawing.Size(174, 20);
            this.courtRateEdit.TabIndex = 4;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(98, 250);
            this.btnDone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(85, 25);
            this.btnDone.TabIndex = 5;
            this.btnDone.Text = "Save";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(9, 250);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 25);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Last name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "First name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Patronymic";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 155);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Adress";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 203);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Court rate";
            // 
            // AddPeople
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 286);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.courtRateEdit);
            this.Controls.Add(this.adressEdit);
            this.Controls.Add(this.patronymicEdit);
            this.Controls.Add(this.firstNameEdit);
            this.Controls.Add(this.lastNameEdit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AddPeople";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add people";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox lastNameEdit;
        private System.Windows.Forms.TextBox firstNameEdit;
        private System.Windows.Forms.TextBox patronymicEdit;
        private System.Windows.Forms.TextBox adressEdit;
        private System.Windows.Forms.TextBox courtRateEdit;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}