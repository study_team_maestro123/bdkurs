﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Accidents
{
    public partial class AccidentCreateForm : Form
    {
        public AccidentCreateForm()
        {
            InitializeComponent();
            btnDone.Enabled = false;
            btnDone.Click += onDoneClick;
        }

        private void typeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            verifyDoneButton();
        }

        private void verifyDoneButton()
        {
            btnDone.Enabled = typeList.SelectedIndex > -1;
        }

        private void onDoneClick(object sender, EventArgs e)
        {
            using (SqlCommand cmd = DbController.Instance.Command("INSERT INTO [AccidentTbl] (fab,regDate) VALUES (@0,@1)"))
            {
                DateTime date = DatePicker.Value.Date;
                cmd.Parameters.AddWithValue("@0", typeList.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@1", new DateTime(date.Year, 
                                                               date.Month, 
                                                               date.Day, 
                                                               TimePicker.Value.Hour, 
                                                               TimePicker.Value.Minute, 
                                                               TimePicker.Value.Second
                                                               ));
                cmd.ExecuteNonQuery();
                Close();
            }
            DbController.Instance.NotifyDataLoadRequired();
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
