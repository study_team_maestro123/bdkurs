﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Xml;

namespace Accidents
{

    class DbController
    {

        private static DbController instance;
        
        private DbController()
        {
        }

        public static DbController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DbController();
                }
                return instance;
            }
        }

        private static String[] RELATIONS = new String[] { "Сulprit", "Victim", "Suspected", "Eyewitness" };
        private static String[] STATUS = new String[] { "Approved", "Denied" };

        private SqlConnection mConnection = new SqlConnection(Properties.Settings.Default.AccidentDBConnectionString);
        private List<OnDataLoadRequiredListener> mListeners = new List<OnDataLoadRequiredListener>();
        private bool isInitialize = false;

        public void AddListener(OnDataLoadRequiredListener listener)
        {
            mListeners.Add(listener);
        }

        public void RemoveListener(OnDataLoadRequiredListener listener)
        {
            mListeners.Remove(listener);
        }

        public void Start()
        {
            Debug.WriteLine("Connection string: " + Properties.Settings.Default.AccidentDBConnectionString);
            mConnection.Open();
            mConnection.InfoMessage += (object obj, SqlInfoMessageEventArgs e) => {
                Debug.WriteLine(e.Message);
            };
            if (!isInitialize)
            {
                fillStatus();
                fillRelations();
                fillPeoples();
            }
        }

        public void Stop()
        {
            mConnection.Close();
        }
        
        public SqlCommand Command(String sql)
        {
            return new SqlCommand(sql, mConnection);
        }

        public void NotifyDataLoadRequired()
        {
            foreach (OnDataLoadRequiredListener listener in mListeners)
            {
                listener.OnDataLoadRequired();
            }
        }

        public void fillPeoples()
        {
            using (XmlTextReader reader = new XmlTextReader("C:\\Users\\psevd\\Documents\\Visual Studio 2015\\Projects\\bdkurs\\Accidents\\peoples.xml"))
            {
                using (SqlCommand command = Command("INSERT INTO [PeopleTbl] VALUES (@lastname,@firstname,@patronymic,@adress,@courtRate)"))
                {
                    string text = null;
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (reader.Name.Equals(PARAM_MAN))
                                {
                                    command.Parameters.Clear();
                                }
                                break;
                            case XmlNodeType.Text:
                                text = reader.Value;
                                break;
                            case XmlNodeType.EndElement:
                                if (reader.Name.Equals(PARAM_MAN))
                                {
                                    command.ExecuteNonQuery();
                                }
                                else if (!reader.Name.Equals(PARAM_PEOPLES))
                                {
                                    command.Parameters.AddWithValue("@" + reader.Name, text);
                                }
                                break;
                        }
                    }
                }
            }
        }

        private void fillRelations()
        {
            using (SqlCommand command = Command("INSERT INTO [RelationTbl] VALUES (@1)"))
            {
                foreach (String relation in RELATIONS)
                {
                    command.Parameters.AddWithValue("@1", relation);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
            }
        }

        private void fillStatus()
        {
            using (SqlCommand command = Command("INSERT INTO [StatusTbl] VALUES (@1)"))
            {
                foreach (String status in STATUS)
                {
                    command.Parameters.AddWithValue("@1", status);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
            }
        }

        public interface OnDataLoadRequiredListener
        {
            void OnDataLoadRequired();
        }

        public static string QUERY_ACCIDENTS = "SELECT * FROM [AccidentTbl]";
        public static string QUERY_ACCIDENTS_INFO = "SELECT * FROM [AccidentInfoTbl] info INNER JOIN [AccidentTbl] acc ON (info.regId = acc.regId) LEFT JOIN [StatusTbl] status ON (info.status = status.pid)";
        public static string QUERY_PEOPLES = "SELECT * FROM [PeopleTbl]";
        public static string QUERY_IMPLICATIONS = "SELECT * FROM [ImplicationTbl] implication INNER JOIN [PeopleTbl] people ON (implication.manRegId = people.regId) INNER JOIN [AccidentInfoTbl] accInfo ON (implication.acRegId = accInfo.regId) INNER JOIN [RelationTbl] relation ON (implication.status = relation.pid)";

        private static string PARAM_MAN = "man";
        private static string PARAM_PEOPLES = "peoples";

        private AccidentSort mAccidentSort = AccidentSort.Id;
        private PeopleSort mPeopleSort = PeopleSort.Id;

        public AccidentSort getAccidentSort()
        {
            return mAccidentSort;
        }

        public void setAccidentSort(AccidentSort sort) {
            mAccidentSort = sort;
            NotifyDataLoadRequired();
        }

        public PeopleSort getPeopleSort()
        {
            return mPeopleSort;
        }

        public void setPeopleSort(PeopleSort sort)
        {
            Debug.WriteLine("Set sort: " + sort);
            mPeopleSort = sort;
            NotifyDataLoadRequired();
        }

        public enum AccidentSort
        {
            Id, Type, Date
        }

        public static string AccidentSortSql(AccidentSort sort)
        {
            switch (sort)
            {
                case AccidentSort.Type:
                    return "fab";
                case AccidentSort.Date:
                    return "regDate";
                case AccidentSort.Id:
                    return "regId";
            }
            return null;
        }

        public enum PeopleSort
        {
            Id, Firstname, Lastname, Patronymic, Adress, CourtRate
        }

        public static string PeopleSortSql(PeopleSort sort)
        {
            switch (sort)
            {
                case PeopleSort.Id:
                    return "regId";
                case PeopleSort.Lastname:
                    return "lastname";
                case PeopleSort.Firstname:
                    return "firstname";
                case PeopleSort.Patronymic:
                    return "patronymic";
                case PeopleSort.Adress:
                    return "adress";
                case PeopleSort.CourtRate:
                    return "courtRate";
            }
            return null;
        }
        
    }
}
